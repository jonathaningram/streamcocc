package streamcocc

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func JSONResponse(rw http.ResponseWriter, response interface{}, code int) {
	jsonRes, err := json.Marshal(response)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json; charset=utf-8")
	rw.WriteHeader(code)
	fmt.Fprintln(rw, string(jsonRes[:]))
}
