package streamcocc

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

var ErrJSONParsingFailed = errors.New("Could not decode request: JSON parsing failed")

type MainRequest struct {
	// The payload is a slice of Show
	// Note: it's unclear if the incoming data is structured/unstructured and so
	// we can't account for unknown fields in this struct. Using something like
	// map[string]interface{} would be something else to consider here.
	Payload []Show `json:"payload"`
}

func init() {
	http.HandleFunc("/", mainHandler)
}

func mainHandler(rw http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		rw.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	reqPayload := &MainRequest{}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(body, reqPayload)
	if err != nil {
		JSONResponse(rw, map[string]string{
			"error": ErrJSONParsingFailed.Error(),
		}, http.StatusBadRequest)
		return
	}

	filteredShows := FilterDRMAndEpisodes(reqPayload.Payload)

	JSONResponse(rw, map[string][]MinimalShow{
		"response": filteredShows,
	}, http.StatusOK)
}
