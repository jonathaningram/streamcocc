package streamcocc

import "time"

type Show struct {
	Country       string    `json:"country"`
	Description   string    `json:"description"`
	DRM           bool      `json:"drm"`
	EpisodeCount  uint      `json:"episodeCount"`
	Image         ShowImage `json:"image"`
	Genre         string    `json:"genre"`
	Language      string    `json:"language"`
	NextEpisode   *Episode  `json:"nextEpisode"`
	PrimaryColour string    `json:"primaryColour"`
	Seasons       []struct {
		Slug string `json:"slug"`
	} `json:"seasons"`
	Slug      string `json:"slug"`
	Title     string `json:"title"`
	TVChannel string `json:"tvChannel"`
}

type ShowImage struct {
	ShowImage string `json:"showImage"`
}

type Episode struct {
	Channel     string     `json:"channel"`
	ChannelLogo string     `json:"channelLogo"`
	Date        *time.Time `json:"date"`
	HTML        string     `json:"html"`
	URL         string     `json:"url"`
}

// Struct name could be better, but good enough with given context.
type MinimalShow struct {
	Image string `json:"image"`
	Slug  string `json:"slug"`
	Title string `json:"title"`
}

func NewMinimalShowFromShow(s Show) MinimalShow {
	return MinimalShow{
		Image: s.Image.ShowImage,
		Slug:  s.Slug,
		Title: s.Title,
	}
}

// Func name could probably be better.
func FilterDRMAndEpisodes(shows []Show) []MinimalShow {
	mShows := make([]MinimalShow, 0, len(shows))

	for _, show := range shows {
		if !show.DRM {
			continue
		}
		if show.EpisodeCount == 0 {
			continue
		}

		mShows = append(mShows, NewMinimalShowFromShow(show))
	}

	return mShows
}
