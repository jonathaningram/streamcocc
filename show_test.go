package streamcocc

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"
)

func TestFilterDRMAndEpisodes(t *testing.T) {
	tests := []struct {
		shows  []Show
		mShows []MinimalShow
	}{
		// Empty
		{
			shows:  []Show{},
			mShows: []MinimalShow{},
		},
		// "drm" is false, and episodes
		{
			shows: []Show{
				{
					DRM:          false,
					EpisodeCount: 0,
				},
			},
			mShows: []MinimalShow{},
		},
		// "drm" is true, but no episodes
		{
			shows: []Show{
				{
					DRM:          true,
					EpisodeCount: 0,
				},
			},
			mShows: []MinimalShow{},
		},
		// "drm" is false, but has episodes
		{
			shows: []Show{
				{
					DRM:          false,
					EpisodeCount: 5,
				},
			},
			mShows: []MinimalShow{},
		},
		// "drm" is true and has episodes
		{
			shows: []Show{
				{
					DRM:          true,
					EpisodeCount: 5,
					Image: ShowImage{
						ShowImage: "http://snail.com/img.png",
					},
					Slug:  "snail",
					Title: "Snail",
				},
			},
			mShows: []MinimalShow{
				{
					Image: "http://snail.com/img.png",
					Slug:  "snail",
					Title: "Snail",
				},
			},
		},
	}

	for i, tt := range tests {
		actual := FilterDRMAndEpisodes(tt.shows)
		if !reflect.DeepEqual(actual, tt.mShows) {
			t.Errorf("%d. FilterDRMAndEpisodes(%#v) = %#v; want %#v", i, tt.shows, actual, tt.mShows)
		}
	}
}

func TestJSONEncodeMinimalShow(t *testing.T) {
	tests := []struct {
		show MinimalShow
		json []byte
	}{
		// Empty
		{
			show: MinimalShow{
				Image: "",
				Slug:  "",
				Title: "",
			},
			json: []byte(`{"image":"","slug":"","title":""}`),
		},
		// Normal
		{
			show: MinimalShow{
				Image: "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg",
				Slug:  "show/16kidsandcounting",
				Title: "16 Kids and Counting",
			},
			json: []byte(`{"image":"http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg","slug":"show/16kidsandcounting","title":"16 Kids and Counting"}`),
		},
	}

	for i, tt := range tests {
		b, err := json.Marshal(tt.show)
		if err != nil {
			t.Fatal(err)
		}
		if bytes.Compare(b, tt.json) != 0 {
			t.Errorf("%d. jsonMarshal(%#v) = %s; want %s", i, tt.show, b, tt.json)
		}
	}
}
