# Running the app

The app is designed to run in Google App Engine. Once you have GAE installed,
it's as simple as running a command to get the app deployed.

## Development

```
$ goapp serve
```

## Production

```
$ goapp deploy
```

# Running the tests

```
$ go test
```

Alternatively, `goconvey` can be used to run the tests in a web UI.
